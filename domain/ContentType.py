from abc import ABCMeta


class ContentType:
    """Abstract class for Content Types"""
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @classmethod
    @staticmethod
    def get_name():
        """Returns the name of this type of content"""
        raise Exception("Not Implemented")

    @classmethod
    @staticmethod
    def extract(text):
        """ If it matches the criteria, extracts the content type from the text
            If no match, returns None

            Keyword arguments:
            text -- the text to be examined
        """
        raise Exception("Not Implemented")