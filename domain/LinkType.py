import validators
import urllib2
from bs4 import BeautifulSoup
from domain.ContentType import ContentType


class LinkType(ContentType):

    def __init__(self):
        pass

    @staticmethod
    def get_name():
        """Returns the name of this type of content"""
        return "links"

    @staticmethod
    def extract(text):
        """ If it matches the criteria, extracts the content type from the text
            If no match, returns None

            The criteria is the text must be a valid url.

            Keyword arguments:
            text -- the text to be examined

            Returns
                tuple containing the url and web page title
        """
        m = None
        if validators.url(text):
            m = (LinkResponse(text)).serialize()
        return m


class LinkResponse:

    def __init__(self, url):
        self.url = url
        try:
            self.value = BeautifulSoup(urllib2.urlopen(url), "html.parser").title.string
        except urllib2.URLError:
            self.value = ""

    def serialize(self):
        return {'url': self.url, 'title': self.value}