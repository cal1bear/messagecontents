import re
from domain.ContentType import ContentType


class MentionType(ContentType):

    def __init__(self):
        pass

    @staticmethod
    def get_name():
        """Returns the name of this type of content"""
        return "mentions"

    @staticmethod
    def extract(text):
        """ If it matches the criteria, extracts the content type from the text
            If no match, returns None

            The criteria is the text must start with a '@' and be followed by a string
            of minimum length 1

            Keyword arguments:
            text -- the text to be examined

            Returns the string following the '@'
        """
        m = re.search(r'@(.+)$', text)
        if m is None:
            return None
        return m.group(1)