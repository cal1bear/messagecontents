import re
from domain.ContentType import ContentType


class EmoticonType(ContentType):

    @staticmethod
    def get_name():
        """Returns the name of this type of content"""
        return "emoticons"

    @staticmethod
    def extract(text):
        """ If it matches the criteria, extracts the content type from the text
            If no match, returns None

            The criteria is the text must start with a '(' and end with a ').
            Also there must be a alphanumeric string with length greater than
            zero and less than 16.

            Keyword arguments:
            text -- the text to be examined

            Returns:
            if matches criteria, returns the string between the parenthesis.
            Else returns none
        """
        m = re.search(r'^\(([a-zA-Z0-9]{1,15})\)$', text)
        if m is None:
            return None
        return m.group(1)