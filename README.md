# README #

RESTful API that takes a chat message string as input and returns a JSON object containing information about its contents

### How do I get set up? ###

* Python 2.7
* Dependencies
	* pip install flask
	* pip install validators
	* pip install bs4
* to run the server, python application.py
* sample call 
  ```curl -X POST \
  localhost:5000/contents \
  -H 'content-type: application/json' \
  -d '{"text":"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"}'```

### Approach ###

1. My first decision was to select a python web framework with support for creating restful APIs. There are a lot of options, but I chose flask because it was one of the more lightweight options.
2. For emoticons and mentions, I used the builtin regex library to match and extract the contents.
3. For the links, I chose to use the validators library to validate the text is a url and BeautifulSoup to extract the title. I chose to do this for efficiency in not writing the same code.