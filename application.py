from flask import Flask, request, jsonify
from flask_restful import Api
from exception.BadRequestException import BadRequestException
from domain.MentionType import MentionType
from domain.LinkType import LinkType
from domain.EmoticonType import EmoticonType

app = Flask(__name__)
app.config.from_object(__name__)
api = Api(app)


@app.errorhandler(BadRequestException)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


@app.route('/contents', methods=['POST'])
def extract_contents():

    # list of all content types that will be evaluated
    content_types = [MentionType, EmoticonType, LinkType]

    request_param_name = 'text'

    if not request.json or request_param_name not in request.json:
        raise BadRequestException('\'{}\' parameter missing in request body'.format(request_param_name))

    # dictionary containing all extracted content
    content = {}
    for content_type in content_types:
        content[content_type.get_name()] = []

    for token in request.json[request_param_name].split():
        for content_type in content_types:
            c = content_type.extract(token)
            if c:
                content[content_type.get_name()].append(c)
                continue

    return jsonify(content)


if __name__ == '__main__':
    app.run()
